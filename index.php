<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

$socialLinks = [
  'tripadvisor' => 'https://www.tripadvisor.it/Restaurant_Review-g194754-d3726893-Reviews-Scarlett_Food_Pizza-Empoli_Tuscany.html',
  'facebook' => 'https://www.facebook.com/scarlettfoodpizza/',
  'instagram' => 'https://instagram.com/scarlettpizzatiramisu?igshid=2a3njuxaf1k9'
];

$page = (isset($_GET['page'])) ? $_GET['page'] : 0;
$imagesPath = 'assets/images/';
$siteUrl = 'http://scarlett.brandboosterdemo.com/';
#$siteUrl = 'http://localhost/scarlett/';
include 'commons/products-list.php';
include 'libs/Mobile_Detect.php';
$detect = new Mobile_Detect;
function codifyString($string){
  $string = str_replace('<br>','-',$string);
  preg_replace('/[^A-Za-z0-9\-]/', '', $string);
  $string = str_replace(' ', '-', strtolower($string));
  $string = strip_tags($string);
  return $string;
}
$darkBookingForm = false;
?>
<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/js/fullpage/src/fullpage.css">
  <script type="text/javascript" src="assets/js/fullpage/vendors/scrolloverflow.min.js"></script>
  <!--<script type="text/javascript" src="assets/js/slick/slick.min.js"></script>-->

  <link rel="stylesheet" href="assets/js/slick/slick.css">
  <link rel="stylesheet" href="assets/js/slick/slick-theme.css">
  <script src="assets/js/fullpage/src/fullpage.js"></script>
  <script src="assets/js/fullpage/fullpage.resetSliders.min.js"></script>
  <script src="assets/js/fullpage/dist/fullpage.extensions.min.js"></script>

  <link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,500,600,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="assets/fonts/homemadeapple-regular/stylesheet.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <title>Scarlett</title>
  <style media="screen">

  </style>
  <script type="text/javascript">

  var imagesPath = '<?=$imagesPath?>';
  </script>
  <style media="screen">

  .fp-controlArrow.fp-prev {
    left: 60px;
    border: none;
    width: 50px;
    height: 101px;
    background-image: url('<?=$siteUrl.$imagesPath?>arrow-left.png') ;
    background-repeat: no-repeat;
    opacity: 0.5;
    cursor: pointer;
  }
  .fp-controlArrow.fp-next {
    right: 60px;
    border: none;
    width: 50px;
    height: 101px;
    background-image: url('<?=$siteUrl.$imagesPath?>arrow-right.png') ;
    background-repeat: no-repeat;
    opacity: 0.5;
    cursor: pointer;
  }

  </style>
</head>
<body>
  <?php include 'commons/navbar.php'; ?>

  <div class="leftBar">

  </div>
  <div class="rightBar d-none d-md-block">
    <div class="btnMenu btnMenuTira">
      <img src="<?=$imagesPath?>icon-menu.png" alt="">Menu Tiramisù
    </div>
    <div class="btnMenu btnMenuPizze" onclick="$('.overlayMenu').slideToggle()">
      <img src="<?=$imagesPath?>icon-menu.png" alt="">Menu Pizze
    </div>
    <div class="btnMenu btnMenuPizze" style="padding: 10px; background-color: #fff; height: 40px;">
      <a class="red" href="tel:+390571501990"><img src="<?=$imagesPath?>icons8-add_phone.png" alt=""></a>
    </div>
  </div>
  <?php include 'pages/'.$page.'/section.php'?>
  <?php include 'commons/pizzas-menu.php'; ?>
  <?php
  $scrollToTopWhere = (isset($scrollToTopWhere)) ? $scrollToTopWhere : 'body' ;
  ?>
  <div class="scrollToTopButton" onclick="scrollToDiv('<?=$scrollToTopWhere?>')"><?php include 'commons/carousel-arrow-right.php'; ?></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js" type="text/javascript"></script>
<script src="assets/js/scripts.js?v=<?=rand(0,10000000)?>" type="text/javascript"></script>
</body>
</html>
