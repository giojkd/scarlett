<?php
$products = [
  [
    'showIn' => ['pizzas','all'],
    'category_name' => 'Le nostre<br>rosse',
    'products' => [
      [
        'title' => 'La Tartufina',
        'description' => 'Pomodoro san marzano dop<br> mozzarella fior di latte, tartufo di San Miniato, olio tartufato, burrata a fine cottura',
        'price' => '14,00',
        'ingredients' => 1
      ],
      [
        'title' => 'La Taggiasca',
        'description' => 'Pomodoro san marzano dop<br> capperi di lipari, acciughe del cantabrico, origano, olive taggiasche, olio evo, burrata a fine cottura',
        'price' => '12,50',
        'ingredients' => 1
      ],
      [
        'title' => 'La Sbufalata cantabrica',
        'description' => 'Pomodoro san marzano dop<br> bufala a fette & acciughe del cantabrico',
        'price' => '11,00',
        'ingredients' => 1
      ],
      [

        'title' => 'La Salamino & burrata',
        'description' => 'Pomodoro san marzano dop<br> salamino, burrata',
        'price' => '9,50',
        'ingredients' => 1
      ],
      [

        'title' => 'La Napoletana',
        'description' => 'Mozzarella fior di latte, Pomodoro san marzano dop<br> acciughe del cantabrico e capperi di lipari',
        'price' => '9,50',
        'ingredients' => 1
      ],
      /*  [

      'title' => 'La Calabrese',
      'description' => 'Provola, mozzarella fior di latte, n’duja di spiliniga, pomodoro san marzano dop',
      'price' => '10,00',
      'ingredients' => 1
    ], */
    [

      'title' => 'La San Marzano Dop',
      'description' => 'Mozzarella fiordilatte, pomodoro san marzano dop & basilico',
      'price' => '8,50',
      'ingredients' => 1
    ]
  ]
],
[
  'showIn' => ['pizzas','all'],
  'category_name' => 'Le nostre<br>scrocchiarelle',
  'products' => [
    [

      'title' => 'La Pregiata',
      'description' => 'Cuore di burrata, prosciutto crudo e tartufo a scaglie',
      'price' => '13,00',
      'ingredients' => 1
    ],
    /*[

    'title' => 'La Salsiccia & Stracchino',
    'description' => 'Salsiccia e Stracchino',
    'price' => '9,00',
    'ingredients' => 1
  ],
  [

  'title' => 'L\'Italiana',
  'description' => 'Pomodoro san marzano dop<br> mozzarella e rucola',
  'price' => '8,50',
  'ingredients' => 1
  ]*/
]
],
[
  'showIn' => ['pizzas','all'],
  'category_name' => 'Le nostre<br>bianche',
  'products' => [
    [

      'title' => 'La Carciofara',
      'description' => 'Mozzarella fiordilatte, crema di carciofi morelli, n’duia di spiliniga e pecorino romano',
      'price' => '12,00',
      'ingredients' => 1
    ],
    [

      'title' => 'La Zucchetta',
      'description' => 'Crema di zucca, mozzarella di bufala, guanciale',
      'price' => '11,50',
      'ingredients' => 1
    ],
    [

      'title' => 'La Piera',
      'description' => 'Mozzarella fior di latte, speck, gorgonzola, noci, miele millefiori',
      'price' => '12,00',
      'ingredients' => 1
    ],
    [

      'title' => 'La Fior di Cacio',
      'description' => 'Mozzarella fiordilatte, fiori di zucca, taleggio e pepe nero affumicato al melo',
      'price' => '9,50',
      'ingredients' => 1
    ],
    /*  [

    'title' => 'La Porrina',
    'description' => 'Mozzarella fior di latte, crema di porri, salsiccia pinzani e porro croccante',
    'price' => '10,00',
    'ingredients' => 1
  ],*/
  [

    'title' => 'La Veggie',
    'description' => 'Mozzarella fior di latte, basilico fresco, radicchio, zucchine al forno, pomodorino giallo',
    'price' => '9,50',
    'ingredients' => 1
  ]
]
],
[
  'showIn' => ['tiramisu','all'],
  'category_name' => 'I Nostri<br>Il Tiramisu',
  'products' => [
    [

      'title' => '1 Tiramisu',
      'description' => 'MASCARPONE, BISCOTTI SAVOIARDI, ZUCCHERO DI CANNA, UOVA FRESCHE, CAFFE’, CACAO IN POLVERE',
      'price' => '8,50',
      'ingredients' => 1
    ],
    [

      'title' => '2 Tiramisu',
      'description' => 'MASCARPONE, BISCOTTI SAVOIARDI, ZUCCHERO DI CANNA, UOVA FRESCHE, CAFFE’, CACAO IN POLVERE',
      'price' => '8,50',
      'ingredients' => 1
    ],
    [

      'title' => '3 Tiramisu',
      'description' => 'MASCARPONE, BISCOTTI SAVOIARDI, ZUCCHERO DI CANNA, UOVA FRESCHE, CAFFE’, CACAO IN POLVERE',
      'price' => '8,50',
      'ingredients' => 1
    ],
    [

      'title' => '4 Tiramisu',
      'description' => 'MASCARPONE, BISCOTTI SAVOIARDI, ZUCCHERO DI CANNA, UOVA FRESCHE, CAFFE’, CACAO IN POLVERE',
      'price' => '8,50',
      'ingredients' => 1
    ],
    [

      'title' => '5 Tiramisu',
      'description' => 'MASCARPONE, BISCOTTI SAVOIARDI, ZUCCHERO DI CANNA, UOVA FRESCHE, CAFFE’, CACAO IN POLVERE',
      'price' => '8,50',
      'ingredients' => 1
    ],
    [

      'title' => '6 Tiramisu',
      'description' => 'MASCARPONE, BISCOTTI SAVOIARDI, ZUCCHERO DI CANNA, UOVA FRESCHE, CAFFE’, CACAO IN POLVERE',
      'price' => '8,50',
      'ingredients' => 1
    ]
  ]
]


];
?>
