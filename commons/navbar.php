<nav id="mainNavbar" class="d-none d-md-block navbar fixed-top navbar-expand-md navbar-dark navbar-top-padder">
  <div class="container">
    <div class="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
      <ul class="navbar-nav mr-auto d-flex d-flex justify-content-around w-100">
        <li class="nav-item nav-item-social">
          <a target="_blank" class="nav-link nav-link-icon d-inline-block" href="<?=$socialLinks['tripadvisor']?>">
            <span class="social-link red"><i class="fab fa-tripadvisor"></i></span>
          </a>
          <a target="_blank" class="nav-link nav-link-icon d-inline-block" href="<?=$socialLinks['facebook']?>">
            <span class="social-link red"><i class="fab fa-facebook"></i></span>
          </a>
          <a target="_blank" class="nav-link nav-link-icon d-inline-block" href="<?=$socialLinks['instagram']?>">
            <span class="social-link red"><i class="fab fa-instagram"></i></span>
          </a>
        </li>
        <li class="nav-item <?=($page == 1) ? 'active' : ''?>">
          <a id="ni_ssi_1_0" class="nav-link" href="<?=$siteUrl?>?page=1">Team</a>
        </li>
        <li class="nav-item <?=($page == 2) ? 'active' : ''?>">
          <a id="ni_ssi_2_0" class="nav-link" href="<?=$siteUrl?>?page=2">Perché è speciale</a>
        </li>
      </ul>
    </div>
    <div class="mx-auto order-0">
      <a class="navbar-brand mx-auto" href="#">
        <div class="logo">
          <a href="<?=$siteUrl?>">
            <img id="logoImg" src="<?=$imagesPath?>logo-white.png" alt="">
          </a>
        </div>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-collapse2">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
      <ul class="navbar-nav w-100 d-flex justify-content-around">
        <li class="nav-item <?=($page == 3) ? 'active' : ''?>">
          <a id="ni_ssi_3_0" class="nav-link" href="<?=$siteUrl?>?page=3">Menù</a>
        </li>
        <li class="nav-item <?=($page == 4) ? 'active' : ''?>">
          <a id="ni_ssi_4_0" class="nav-link" href="<?=$siteUrl?>?page=4">Gallery</a>
        </li>
        <li class="nav-item <?=($page == 5) ? 'active' : ''?>">
          <a id="ni_ssi_5_0" class="nav-link" href="<?=$siteUrl?>?page=5">Contatti</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<!-- NAVBAR MOBILE -->

<nav id="mainNavbarMobile" class="d-block d-sm-none navbar fixed-top navbar-expand-lg navbar-light ">
  <div class="mr-auto">
    <div class="mobileNavbarSocialLinks mr-auto">
      <a target="_blank" class="" href="<?=$socialLinks['tripadvisor']?>">
        <span class="red"><i class="fab fa-tripadvisor"></i></span>
      </a>
      <a target="_blank" class="" href="<?=$socialLinks['facebook']?>">
        <span class="red"><i class="fab fa-facebook"></i></span>
      </a>
      <a target="_blank" class="" href="<?=$socialLinks['instagram']?>">
        <span class="red"><i class="fab fa-instagram"></i></span>
      </a>
    </div>
  </div>


  <div class="mobileNavbarLogo">
    <a href="<?=$siteUrl?>">
      <img src="<?=$imagesPath?>logo-white.png" alt="">
    </a>
  </div>


  <div class="ml-auto text-right">
    <button class="navbar-toggler red p-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="red"><i class="fas fa-bars"></i></span>
    </button>
  </div>


  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item <?=($page == 0) ? 'active' : ''?>">
        <a id="ni_ssi_0_0" class="nav-link" href="<?=$siteUrl?>">Home</a>
      </li>
      <li class="nav-item <?=($page == 1) ? 'active' : ''?>">
        <a id="ni_ssi_1_0" class="nav-link" href="<?=$siteUrl?>?page=1">Team</a>
      </li>
      <li class="nav-item <?=($page == 2) ? 'active' : ''?>">
        <a id="ni_ssi_2_0" class="nav-link" href="<?=$siteUrl?>?page=2">Perché è speciale</a>
      </li>
      <li class="nav-item <?=($page == 3) ? 'active' : ''?>">
        <a id="ni_ssi_3_0" class="nav-link" href="<?=$siteUrl?>?page=3">Menu</a>
      </li>
      <li class="nav-item <?=($page == 4) ? 'active' : ''?>">
        <a id="ni_ssi_4_0" class="nav-link" href="<?=$siteUrl?>?page=4">Gallery</a>
      </li>
      <li class="nav-item <?=($page == 5) ? 'active' : ''?>">
        <a id="ni_ssi_5_0" class="nav-link" href="<?=$siteUrl?>?page=5">Contatti</a>
      </li>
    </ul>
  </div>
</nav>
