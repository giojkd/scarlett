<div class="p-5" style="background-size:cover; background-position: center bottom; background-repeat:no-repeat; background-image:url('<?=$siteUrl.$imagesPath?>background-4-0.png')">
  <div class="row">
    <div class="col-md-10 offset-md-1">
      <div class="reviewsBox">
        <div class="row">
          <?php foreach($reviews as $reviewIndex =>  $review){?>
            <div class="col <?=($reviewIndex > 0) ? 'd-none d-md-block' : ''?>">
              <div class="reviewBox">
                <div class="reviewRibbon">
                  <img src="<?=$imagesPath?>review-box-ribbon.png" alt="">
                </div>
                <div class="row">
                  <div class="col">
                    <div class="reviewUser">
                      <img src="<?=$imagesPath?>review-box-user.png" alt="">
                    </div>

                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="reviewStars">
                      <?php for($i = 0; $i < $review['stars']; $i++){?>
                        <img src="<?=$imagesPath?>review-box-star-ok.png" alt="">
                      <?php }?>
                      <?php for($i = 0; $i < (5-$review['stars']); $i++){?>
                        <img src="<?=$imagesPath?>review-box-star-ko.png" alt="">
                      <?php }?>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col">
                    <p class="reviewContent"><?=$review['content']?></p>
                  </div>
                </div>
                <hr>
                <p class="reviewerName"><?=$review['reviewer']['name']?></p>
                <p class="reviewerTitle"><?=$review['reviewer']['title']?></p>
              </div>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>
