<div class="d-flex vh100 vw100">
  <div class="align-self-center w-100">
    <div id="<?=$carouseSlidesName?>" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <?php foreach($carouseSlides as $index => $carouselSlide){?>
          <div class="carousel-item <?=($index == 0) ? 'active' : ''?>">
            <div class="row">
              <div class="col-md-6 offset-md-3 text-center">
                <img class="img-fluid-80" src="<?=$carouselSlide['img']?>" class="d-block w-100" alt="...">
              </div>
            </div>
          </div>
        <?php } ?>
      </div>
      <div class="carousel-control-custom-wrapper-wrapper d-none d-md-block">
        <div class="container carousel-control-custom-wrapper ">
          <div class="row">
            <div class="col text-left">
              <a class="carousel-control-custom carousel-control-prev d-none d-md-block" href="#<?=$carouseSlidesName?>" role="button" data-slide="prev">
                <span class="" aria-hidden="true"><?php include 'commons/carousel-arrow-left.php'?></span>
                <span class="sr-only">Previous</span>
              </a>
            </div>
            <div class="col text-right">
              <a class="carousel-control-custom carousel-control-next d-none d-md-block" href="#<?=$carouseSlidesName?>" role="button" data-slide="next">
                <span class="" aria-hidden="true"><?php include 'commons/carousel-arrow-right.php'?></span>
                <span class="sr-only">Next</span>
              </a>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
</div>
