<?php $darkBookingForm = true; ?>
<div class="productsListPage text-center">
  <div class="d-inline-block <?=($detect->isMobile()) ? 'w-100' : 'w-75'?>">

    <div class="container-fluid">
      <div class="row">
        <?php
        $index = 0;
        foreach($products as $category){
          if(in_array($showIn,$category['showIn'])){
            ?>
            <div class="col-md-<?=($showIn == 'all') ? '3' : '4'?> <?=($showPriceAndDescription) ? 'text-left' : 'text-center'?>">
              <div class="productsListListWrapper">
                <h1>
                  <!--<img class="d-none d-md-block-inline" src="<?=$imagesPath?><?=($showPriceAndDescription) ? 'mini-pizza-red.png' : 'mini-pizza.png'?>" alt="">-->
                  <?=$category['category_name']?>
                  <!--<img class="d-block-inline d-sm-none" src="<?=$imagesPath?><?=($showPriceAndDescription) ? 'mini-pizza-red.png' : 'mini-pizza.png'?>" alt="">-->
                </h1>
                <br>
                <ul class="list-unstyled">
                  <?php foreach($category['products'] as $product){
                    ?>

                    <?php if($showPriceAndDescription){?>
                      <li>
                        <div class="row">
                          <div class="col text-left">
                            <span onclick="scrollToDiv('#dish-<?=$index?>')" class="productTitle"><?=$product['title']?></span>
                          </div>

                          <div class="col text-right productPrice">
                            &euro; <?=$product['price']?>
                          </div>

                        </div>
                        <div class="row">
                          <div class="col">
                            <p class="productDescription">
                              <?=ucfirst(strtolower($product['description']))?>
                            </p>
                          </div>
                        </div>
                      </li>
                    <?php }else{
                      ?>
                      <li class="text-center">

                        <span onclick="scrollToDiv('#dish-<?=$index?>')" class="productTitle"><?=$product['title']?></span>

                      </li>
                    <?php }?>



                    <?php $index++; }?>
                  </ul>
                </div>
              </div>
            <?php }
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <?php
  if(!$showPriceAndDescription){
    ?>

    <?php
    $index = 0;
    foreach($products as $category){
      $indexOo = 0;
      if(in_array($showIn,$category['showIn'])){
        ?>
        <div class="productCategoryTitle">
          <img src="<?=$siteUrl.$imagesPath.codifyString($category['category_name'])?>-title.png" alt="">
          <br>
          <div class="verticalSeparator out"></div>
        </div>
        <?php
        if ( $detect->isMobile() ) {
          foreach($category['products'] as  $product){
            $pizzaTitle = codifyString($product['title']);
            $pizzaTitleIngredients = 'composizione-'.substr($pizzaTitle,3);
            ?>
            <div class="pizzaMobile <?=($indexOo % 2 == 0) ? 'bg-white' : 'bg-light-grey'?> text-center" id="dish-<?=$index?>">
              <div class="pizzaMobileHeader">
                <img src="<?=$imagesPath?>pizzas-titles/<?=$pizzaTitle?>.png" alt="" class="pizzaTitle">
                <span class="pizzaHeaderPrice">&euro; <?=$product['price']?></span>
              </div>

              <p class="pizzaMobileDescription"><?=$product['description']?></p>
              <?php for($i = 0; $i< $product['ingredients']; $i++){
                ?>
                <div class="<?=codifyString($product['title'])?>-ingredient-<?=$i?>">
                  <div class="pizzaIngredients">
                    <img src="<?=$imagesPath?>pizzas-photos-new/<?=$pizzaTitleIngredients?>.png" alt="">
                  </div>
                </div>
              <?php }?>
              <img class="pizzaImage" src="<?=$imagesPath?>pizzas-photos-new/<?=$pizzaTitle?>.png" alt="">
            </div>
            <?php
            $index++;
            $indexOo++;
          }
        }


        if (! $detect->isMobile() ) {
          foreach($category['products'] as  $product){

            $pizzaTitle = codifyString($product['title']);
            $pizzaTitleIngredients = 'composizione-'.substr($pizzaTitle,3);
            ?>
            <div class="vh100 <?=($indexOo % 2 == 0) ? 'bg-white' : 'bg-light-grey'?>" id="dish-<?=$index?>">
              <div class="row m-0">
                <?php if($indexOo % 2 == 0){
                  ?>
                  <div class="col-md-5 text-center">
                    <div class="vh100 d-flex justify-content-center">
                      <div class="align-self-center">
                        <div class="pizzaHeader align-self-center ">
                          <img src="<?=$imagesPath?>pizzas-titles/<?=$pizzaTitle?>.png" alt="" class="pizzaTitle">

                          <p><?=$product['description']?></p>
                          <span class="pizzaHeaderPrice">&euro; <?=$product['price']?></span>
                        </div>
                        <br>
                        <?php for($i = 0; $i< $product['ingredients']; $i++){
                          ?>
                          <div class="<?=codifyString($product['title'])?>-ingredient-<?=$i?>">
                            <div class="pizzaIngredients">


                              <img src="<?=$imagesPath?>pizzas-photos-new/<?=$pizzaTitleIngredients?>.png" alt="">
                            </div>
                          </div>

                        <?php }?>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-7 text-center">
                    <div class="vh100 d-flex justify-content-center">
                      <div class="align-self-center">
                        <img class="pizzaImage right" src="<?=$imagesPath?>pizzas-photos-new/<?=$pizzaTitle?>.png" alt="">
                      </div>
                    </div>
                  </div>
                <?php }else{
                  ?>
                  <div class="col-md-7 text-center">
                    <div class="vh100 d-flex justify-content-center">
                      <div class="align-self-center">
                        <img class="pizzaImage left" src="<?=$imagesPath?>pizzas-photos-new/<?=$pizzaTitle?>.png" alt="">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-5 text-center">
                    <div class="vh100 d-flex justify-content-center">
                      <div class="align-self-center">
                        <div class="pizzaHeader ">
                          <img src="<?=$imagesPath?>pizzas-titles/<?=$pizzaTitle?>.png" alt="" class="pizzaTitle">

                          <p><?=$product['description']?></p>
                          <span class="pizzaHeaderPrice">&euro; <?=$product['price']?></span>
                        </div>
                        <br>
                        <?php for($i = 0; $i< $product['ingredients']; $i++){
                          ?>
                          <div class="<?=codifyString($product['title'])?>-ingredient-<?=$i?>">
                            <div class="pizzaIngredients">
                              <img src="<?=$imagesPath?>pizzas-photos-new/<?=$pizzaTitleIngredients?>.png" alt="">
                            </div>
                          </div>

                        <?php }?>
                      </div>
                    </div>
                  </div>
                <?php } ?>
              </div>
            </div>
            <?php
            $index++;
            $indexOo++;
          }}?>
        <?php }}}?>
