<div class="pageIntro">

  <?php
  $inVerticalSeparator = isset($inVerticalSeparator) ? $inVerticalSeparator : false;
  $noVerticalSeparator = isset($noVerticalSeparator) ? $noVerticalSeparator : false;
  $textWhite = isset($textWhite) ? $textWhite : false;
    if($noVerticalSeparator){

    }else{
      ?>
        <div class="verticalSeparator <?=($inVerticalSeparator) ? '' : 'out'?>"></div>
      <?php
    }
  ?>

  <h1 class="<?=($textWhite) ? 'text-white' : ''?>"><?=$pageIntro['h1']?></h1>
  <h2 class="<?=($textWhite) ? 'text-white' : ''?>"><?=$pageIntro['h2']?></h2>
  <div class="horizontalSeparator"></div>
  <br>
  <p class="<?=($textWhite) ? 'text-white' : ''?>"><?=$pageIntro['p']?></p>
</div>
