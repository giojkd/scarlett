<div class="slideIntro out">
  <div class="container">
    <div class="row">
      <div class="col">
        <h1 class="text-white"><?=$slideIntro['h1']?></h1>
        <h2 class="red">
          <?=$slideIntro['h2']?>
          <div class="icon-scroll-mobile d-block d-sm-none">
            <div class="mouse">
              <div class="wheel"></div>
            </div>
            <!--<div class="icon-arrows">
            <span></span>
          </div>-->
        </div>
      </h2>
      <div class="short-separator"></div>
      <h3 class="text-white"><?=$slideIntro['h3']?></h3>
    </div>
  </div>
</div>
</div>
