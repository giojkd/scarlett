<?php /*
<div class="booking-form <?=($darkBookingForm) ? 'dark' :  ''?>">
<div class="container d-none d-md-block">
<div class="row">
<div class="col">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-location<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="" name="">
<option value="">Dove</option>
</select>
</div>
</div>
</div>
<div class="col">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-persons<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="" name="">
<option value="">1 Persona</option>
</select>
</div>
</div>
</div>
<div class="col">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-calendar<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="" name="">
<option value="">07 / 07 / 19</option>
</select>
</div>

</div>
</div>
<div class="col">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-clock<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="m-auto" name="">
<option value="">21:00</option>
</select>
</div>
</div>
</div>
<div class="col text-center">
<button type="button" class="<?=($darkBookingForm) ? 'btn-red' : 'btn-white' ?>" name="button">Prenota il tuo tavolo</button>
</div>
</div>
</div>
<!-- MOBILE BOOKING FORM -->
<div class="container d-block d-sm-none">
<div class="row">
<!--
<div class="col-6">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-location<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="" name="">
<option value="">Dove</option>
</select>
</div>
</div>
</div>
<div class="col-6">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-persons<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="" name="">
<option value="">1 Persona</option>
</select>
</div>
</div>
</div>
<div class="col-6 mt-4">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-calendar<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="" name="">
<option value="">07 / 07 / 19</option>
</select>
</div>

</div>
</div>
<div class="col-6 mt-4">
<div class="form-element">
<span><img src="<?=$imagesPath?>icon-clock<?=($darkBookingForm) ? '-red' : ''?>.png" alt=""></span>
<div class="select-wrapper">
<select class="m-auto" name="">
<option value="">21:00</option>
</select>
</div>
</div>
</div>-->
<div class="col-12 text-center mt-4">
<button type="button" class="btn-block <?=($darkBookingForm) ? 'btn-red' : 'btn-white' ?>" name="button">Prenota il tuo tavolo</button>
</div>
</div>
</div>
</div>
*/?>

<div class="booking-form <?=($darkBookingForm) ? 'dark' :  ''?>">
  <div class="container d-none d-md-block">
    <div class="row">
      <div class="col text-left">
        <a href="https://form.pienissimo.com/registration" target="_blank" class="new-booking-button"><img style="height:18px; margin-right:5px" src="<?=$imagesPath?>icon-calendar.png" alt=""> Prenota il tuo tavolo a Empoli</a>
      </div>
      <div class="col text-right">
        <a href="https://form.pienissimo.com/registration" target="_blank" class="new-booking-button"><img style="height:18px; margin-right:5px" src="<?=$imagesPath?>icon-calendar.png" alt=""> Prenota il tuo tavolo a Pontedera</a>
      </div>

    </div>
  </div>
  <div class="container d-block d-sm-none">
    <div class="row">
      <div class="col-12 mb-3">
        <a href="https://form.pienissimo.com/registration" target="_blank" class="new-booking-button d-block text-center"><img style="height:18px; margin-right:5px" src="<?=$imagesPath?>icon-calendar.png" alt=""> Prenota il tuo tavolo a Empoli</a>
      </div>
      <div class="col-12">
        <a href="https://form.pienissimo.com/registration" target="_blank" class="new-booking-button d-block mb-2 text-center"><img style="height:18px; margin-right:5px" src="<?=$imagesPath?>icon-calendar.png" alt=""> Prenota il tuo tavolo a Pontedera</a>
      </div>

    </div>
  </div>
</div>
