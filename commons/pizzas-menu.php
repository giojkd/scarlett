<div class="vh100 vw100 overlayMenu text-center">

  <div class="mt-5 d-inline-block <?=($detect->isMobile()) ?  'w-100' : 'w-75'?>" >
    <div class="container-fluid">

    
    <div class="row">
      <div class="col text-left">
        <div class="pizzas-menu-social-links d-none d-md-block">


          <a href="#" class=" text-black ">
            <span class="social-link"><i class="fab fa-tripadvisor"></i></span>
          </a>


          <a href="#" class=" text-black">
            <span class="social-link "><i class="fab fa-facebook"></i></span>
          </a>


          <a href="#" class=" text-black">
            <span class="social-link"><i class="fab   fa-instagram"></i></span>
          </a>
        </div>
      </div>
      <div class="col text-center" >
        <span class="pizzaMenuTitle red">Menù Pizze</span>
      </div>
      <div class="col text-right">
        <div class="d-none d-md-block">
          <a class="close" href="javascript:" onclick="$('.overlayMenu').slideUp()"></a>

        </div>

      </div>
    </div>
  </div>

  </div>

  <?php
  $showPriceAndDescription = true;
  $showIn = 'pizzas';
  include 'commons/products-list-page.php';
  include 'commons/booking-form.php';
  ?>
  <div class="mushroomsAndTomatoes d-none d-md-block">
    <img src="<?=$imagesPath?>mushrooms-and-tomatoes.png" alt="">
  </div>
</div>
