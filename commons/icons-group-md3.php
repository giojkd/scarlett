<div class="iconsGroup">
  <div class="container">
    <div class="row">
      <div class="col-md-10 offset-md-1">
        <div class="row">
          <?php
          foreach($icons as $icon){
            ?>
            <div class="col-12 col-md-3 ">
              <div class="iconElement">
                <img src="<?=$imagesPath.$icon['icon']?>" alt="">
                <h4><?=$icon['title']?></h4>
                <p><?=$icon['content']?></p>
              </div>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
</div>
