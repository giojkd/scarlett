
$.fn.isInViewport = function() {
  var elementTop = $(this).offset().top;
  var elementBottom = elementTop + $(this).outerHeight();

  var viewportTop = $(window).scrollTop();
  var viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

var is_resized = 0;

$(window).on("mousemove",function(e){
  if(is_resized == 0){
    is_resized = 1;
  }
})

function scrollToDiv(div){
  $('html, body').animate({
    scrollTop: $(div).offset().top-70
  }, 750);

}

var myVideo = document.getElementById("scarlettVideo");

var hideArrows = [];
var darkLeftBar = [];
var hideRightBar = [];
function toggleArrows(section, slide){
  var sectionSlideIndex = 'ssi_'+section+'_'+slide;


  if(sectionSlideIndex == 'ssi_1_0'){
    //myVideo.play();
  }
  if(typeof sections[section]['slides'] != 'undefined'){
    $('.slidesNavSlideName').html(sections[section]['slides'][slide]['title']+ '<span class="bottomSquaresSeparator">|</span>');
  }
  if(darkLeftBar.includes(sectionSlideIndex)){
    $('.leftBar, .rightBar, .goHomeBtn, .fp-slidesNav').addClass('dark');
    $('.fp-controlArrow.fp-prev').css({"background-image":"url('"+imagesPath+"/arrow-left-dark.png')"})
    $('.fp-controlArrow.fp-next').css({"background-image":"url('"+imagesPath+"/arrow-right-dark.png')"})
    $('.youtube-link').css({"background-image":"url('"+imagesPath+"/icon-bg-youtube.png')"})
    $('.facebook-link').css({"background-image":"url('"+imagesPath+"/icon-bg-facebook.png')"})
    $('.instagram-link').css({"background-image":"url('"+imagesPath+"/icon-bg-instagram.png')"})
  }   else {
    $('.leftBar, .rightBar, .goHomeBtn, .fp-slidesNav').removeClass('dark');
    $('.fp-controlArrow.fp-prev').css({"background-image":"url('"+imagesPath+"/arrow-left.png')"})
    $('.fp-controlArrow.fp-next').css({"background-image":"url('"+imagesPath+"/arrow-right.png')"})
    $('.youtube-link').css({"background-image":"url('"+imagesPath+"/icon-bg-youtube-white.png')"})
    $('.facebook-link').css({"background-image":"url('"+imagesPath+"/icon-bg-facebook-white.png')"})
    $('.instagram-link').css({"background-image":"url('"+imagesPath+"/icon-bg-instagram-white.png')"})
  }
  if(hideRightBar.includes(sectionSlideIndex)){
    $('.rightBar').hide();
  }else{
    $('.rightBar').show();
  }
  if(hideArrows.includes(sectionSlideIndex)){
    $('.fp-controlArrow').hide();
  }else{
    $('.fp-controlArrow').show();
  }
}

var myFullpage = new fullpage('#fullpage', {
  licenseKey:'6440E212-03364D86-ADFE97B3-361EE154',
  slideSelector: '.fullpageslide',
  scrollOverflow: true,
  resetSliders:true,
  slidesNavigation:true,
  resetSlidersKey: 'YnJhbmRib29zdGVyZGVtby5jb21feDJHY21WelpYUlRiR2xrWlhKeklDcA==',
  afterSlideLoad:function(section, origin, destination, direction){

  },
  afterLoad: function(origin, destination, direction){

    toggleArrows(destination.index, 0);
  },
  afterSlideLoad: function(section, origin, destination, direction){

    toggleArrows(section.index, destination.index);
  },

  onLeave: function(origin, destination, direction){

    section = fullpage_api.getActiveSection();


  },
  onSlideLeave: function(section, origin, destination, direction){



  }
});

function resizeBigSlide(){

  $('.bigSlide').css({height:$(window).height()+'px'})
}


function resizePizzaHeader(){
  $('.pizzaHeader').each(function(){
    var ph = $(this);
    var imgWidth = 0;

    imgWidth = ph.find('img').outerWidth();
    ph.css({
      'width':(imgWidth+60)+'px'
    })

  })
}

var countVisibles = 0;
var countVisiblesSeparators = 0;

function setWhiteBgCheck(){
  setTimeout(function(){
    countVisibles  = 0;

    $('.verticalSeparator').each(function(){
      if($(this).isInViewport()){
        $(this).removeClass('out');
      }
    })

    $('.bg-white').each(function(){
      if($(this).isInViewport()){
        countVisibles++;
      }
    })
    if(countVisibles > 0){
      $('.mobileNavbarLogo').hide();
      $('#mainNavbar').removeClass('navbar-dark').addClass('bg-white-navbar').removeClass('navbar-top-padder').addClass('navbar-v-stretched');
      $('#logoImg').attr('src',imagesPath+'logo-black.png');
    }else{
      $('.mobileNavbarLogo').show();
      $('#mainNavbar').addClass('navbar-dark').removeClass('bg-white-navbar').addClass('navbar-top-padder').removeClass('navbar-v-stretched');
      $('#logoImg').attr('src',imagesPath+'logo-white.png');
    }

    setWhiteBgCheck();
  }, 1000);
}

$(function(){

  resizeBigSlide();


  $(window).scroll(function(){
    if ($(window).scrollTop() > 100) {
      $('.scrollToTopButton').addClass('show');
    } else {
      $('.scrollToTopButton').removeClass('show');
    }
  })
  

  $(window).resize(function(){

    resizeBigSlide();


  })


  const slickSlider = $('.slick-carousel')
  slickSlider
  .slick({
    arrows:false,
    dots: true,
    vertical: true,
    verticalSwiping: true,
    dotsClass:'d-none'
  });

  slickSlider.on('wheel', (function(e) {
    e.preventDefault();
    console.log(e.originalEvent.deltaY);
    if (e.originalEvent.deltaY < -90) {
      $(this).slick('slickNext');
    }
    if (e.originalEvent.deltaY > 90)
    {
      $(this).slick('slickPrev');
    }
  }));




  $('.slideIntro').removeClass('out');

  $('.nav-item:not(.nav-item-social)').each(function(){
    $(this).append('<span class="navItemTopSide navItemSide"></span>');
    $(this).append('<span class="navItemRightSide navItemSide"></span>');
    $(this).append('<span class="navItemBottomSide navItemSide"></span>');
    $(this).append('<span class="navItemLeftSide navItemSide"></span>');
  })

  setWhiteBgCheck();

  setTimeout(function(){
    resizePizzaHeader();
  }, 5000);




  $('.nameTitleWrapper').css({'height':$('.nameTitle').outerHeight()+'px'})
  $('.descrLine').wrap('<div class="descrLineWrapper"></div>');
  $(window).resize(function(){
    is_resized = 1;
  })
  $('.homepageColumns .col-content').hover(function(){
    var bg = $(this).data('background');
    $('.homepageColumns').css({
      'background-image':"url('assets/images/"+bg+"')"
    })
  })
})
