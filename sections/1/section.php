<div class="h-100 w-100 position-relative">
  <div class="mobileScreenFullPageWrapper">

    <?php
    $carouseSlidesName = 'pizzasSlider';
    $carouseSlides = [
      [
        'img' => $imagesPath.'pizzas-photos-new/la-fior-di-cacio.png'
      ],
      [
        'img' => $imagesPath.'pizzas-photos-new/la-genovese.png'
      ],
      [
        'img' => $imagesPath.'pizzas-photos-new/la-napoletana.png'
      ],
      [
        'img' => $imagesPath.'pizzas-photos-new/la-piera.png'
      ],
      [
        'img' => $imagesPath.'pizzas-photos-new/la-san-marzano-dop.png'
      ],
    ];
    ?>

    <div class="mobileScreenDishesCarouselWrapper">
      <?php   include 'commons/fullpage-dishes-carousel.php'; ?>
    </div>

    <?php
    $slideIntro = [
      'h1' => 'Scarlett',
      'h2' => 'Pizza',
      'h3' => 'Pizze speciali per persone speciali'
    ];
    include 'commons/slide-intro.php';
    include 'commons/booking-form.php';
    ?>
  </div>
  <?php include 'commons/icon-scroll.php';?>
</div>
