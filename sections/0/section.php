<div class="h-100 w-100 position-relative">
  <div class="mobileScreenFullPageWrapper">
    <?php
    $slideIntro = [
      'h1' => 'Pizza & Tiramisù',
      'h2' => 'Scarlett',
      'h3' => 'C\'è magia in quello che facciamo'
    ];
    include 'commons/slide-intro.php';
    include 'commons/booking-form.php';
    ?>
  </div>
  <?php include 'commons/icon-scroll.php';?>
</div>
