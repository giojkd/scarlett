<div class="h-100 w-100 position-relative">
  <div class="mobileScreenFullPageWrapper">
    <?php
    $carouseSlidesName = 'tiramisuSlider';
    $carouseSlides = [
      [
        'img' => $imagesPath.'tiramisu/1-tiramisu.png'
      ],
      [
        'img' => $imagesPath.'tiramisu/2-tiramisu.png'
      ],
      [
        'img' => $imagesPath.'tiramisu/3-tiramisu.png'
      ],
      [
        'img' => $imagesPath.'tiramisu/4-tiramisu.png'
      ],
      [
        'img' => $imagesPath.'tiramisu/5-tiramisu.png'
      ],
      [
        'img' => $imagesPath.'tiramisu/6-tiramisu.png'
      ]
    ];
    ?>

    <div class="mobileScreenDishesCarouselWrapper">
      <?php   include 'commons/fullpage-dishes-carousel.php'; ?>
    </div>

    <?php


    $slideIntro = [
      'h1' => 'Scarlett',
      'h2' => 'Tiramisù',
      'h3' => 'Dolci d\'autore'
    ];
    include 'commons/slide-intro.php';
    include 'commons/booking-form.php';
    ?>
  </div>
  <?php include 'commons/icon-scroll.php';?>
</div>
