<div class="bg-white">




    <div style="height:140px;" class="d-none d-md-block"></div>
    <?php
    $noVerticalSeparator = false;
    $inVerticalSeparator = true;
    #$textWhite = true;
    $pageIntro = [
      'h1' => 'Team',
      'h2' => 'Dicono di noi ',
      'p' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.'
    ];
    ?>
    <?php   include 'commons/page-intro.php'; ?>

    <?php

    $reviews = [
      [
        'stars' => 4,
        'content' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.',
        'reviewer' => [
          'name' => 'Rubens Parenti',
          'title' => 'Guest'
        ]
      ],
      [
        'stars' => 4,
        'content' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.',
        'reviewer' => [
          'name' => 'Rubens Parenti',
          'title' => 'Guest'
        ]
      ],
      [
        'stars' => 4,
        'content' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.',
        'reviewer' => [
          'name' => 'Rubens Parenti',
          'title' => 'Guest'
        ]
      ]
    ]
    ?>
    <?php   include 'commons/reviews-boxes.php'; ?>




</div>
