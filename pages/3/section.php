<?php
$slideIntro = [
  'h1' => 'Pizza e Tiramisù',
  'h2' => 'Scarlett',
  'h3' => 'Degusta il nostro menu'
];

$pageIntro = [
  'h1' => 'Menu',
  'h2' => 'Menu Pizze',
  'p' => 'Per le nostre pizze usiamo farina BIO di “tipo 1”, una percentuale di farina integrale e di farro realizzate al 100% con grano coltivato in Italia e macinata a pietra lavica con residuo chimico zero. Inoltre facciamo lievitare e maturare il nostro impasto dalle 24 alle 48 ore, dando cosìalle pizze caratteristiche uniche per gusto e digeribilità.'
];


$showIn = 'all';
?>


<div class="w-100 position-relative bigSlide" style="background-image:url('<?=$siteUrl?>assets/images/imm-menu.png')">
    <div class="mobileScreenFullPageWrapper">
  <?php

  include 'commons/slide-intro.php';
  include 'commons/booking-form.php';

  ?>

</div>
<?php include 'commons/icon-scroll.php';?>
</div>

<div class="bg-white">
  <?php   include 'commons/page-intro.php'; ?>
  <?php $showPriceAndDescription = false; ?>
  <?php   include 'commons/products-list-page.php'; ?>

</div>

<?php $scrollToTopWhere =  '.productsListPage'; ?>
