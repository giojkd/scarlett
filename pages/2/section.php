<?php
$slideIntro = [
  'h1' => 'Pizza e Tiramisù',
  'h2' => 'Scarlett',
  'h3' => 'L\'impasto, la farina, gli ingredienti'
];

$pageIntro = [
  'h1' => 'Perché è speciale',
  'h2' => 'L\'impasto',
  'p' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.'
];

$icons = [
  [
    'icon'  =>'ws-icon-4.png',
    'title' => 'Le nostre<br>farine',
    'content' => 'Grazie alla passione/dedizione di tutto il nostro staff siamo riusciti ad ottenere un prodotto finale di altissimo livello.'
  ],
  [
    'icon' => 'ws-icon-5.png',
    'title' => 'La nostra<br>lievitazione',
    'content' => 'Grazie alla passione/dedizione di tutto il nostro staff siamo riusciti ad ottenere un prodotto finale di altissimo livello.'
  ]
  ,
  [
    'icon' => 'ws-icon-6.png',
    'title' => 'Le nostre<br>materie prime',
    'content' => 'Grazie alla passione/dedizione di tutto il nostro staff siamo riusciti ad ottenere un prodotto finale di altissimo livello.'
  ]
];

?>


<div class="w-100 position-relative bigSlide" style="background-image:url('<?=$siteUrl?>assets/images/imm-perche-speciale.png')">
  <div class="mobileScreenFullPageWrapper">
    <?php
    include 'commons/slide-intro.php';
    include 'commons/booking-form.php';
    ?>
  </div>
  <?php include 'commons/icon-scroll.php';?>
</div>

<div class="bg-white">
  <?php   include 'commons/page-intro.php'; ?>
  <?php   include 'commons/icons-group.php'; ?>
  <img class="img-fluid" src="<?=$imagesPath?>why-special-img.png" alt="">
  <?php

  $pageIntro = [
    'h1' => 'Perché è speciale',
    'h2' => 'Il pomodoro',
    'p' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.'
  ];

  ?>
  <?php   include 'commons/page-intro.php'; ?>
  <img class="img-fluid" src="<?=$imagesPath?>why-special-tomato.png" alt="">
  <?php
  $pageIntro = [
    'h1' => 'Perché è speciale',
    'h2' => 'Gli ingredienti top',
    'p' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.'
  ];

  ?>
  <?php   include 'commons/page-intro.php'; ?>
  <img class="img-fluid" src="<?=$imagesPath?>why-special-top-ingredients.png" alt="">

</div>
