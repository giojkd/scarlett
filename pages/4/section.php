<?php
$slideIntro = [
  'h1' => 'Pizza e Tiramisù',
  'h2' => 'Scarlett',
  'h3' => 'L\'impasto, la farina, gli ingredienti'
];

$imagesCount = 17;

?>




<div class="w-100 position-relative bigSlide" >
  <div class="mobileScreenFullPageWrapper" id="mobileScreenFullPageWrapperGallery">
    <?php

    include 'commons/booking-form.php';
    ?>
    <div class="slick-carousel">

      <?php for($index = 1 ; $index < $imagesCount; $index++){
        ?>
        <div class="vh-100 w-100" style="background-size:cover; background-position: center center; background-repeat: no-repeat; background-image:url('<?=$imagesPath?>gallery/Img_<?=$index?>.png')"></div>
      <?php }?>

    </div>

  </div>
  <?php include 'commons/icon-scroll.php';?>
  <?php include 'commons/icon-scroll-mobile.php';?>
</div>
