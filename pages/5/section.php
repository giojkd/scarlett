<?php
$slideIntro = [
  'h1' => 'Pizza e Tiramisù',
  'h2' => 'Scarlett',
  'h3' => 'Scegli la tua location preferita'
];

?>


<div class="w-100 position-relative bigSlide" style="background-image:url('<?=$siteUrl?>assets/images/imm-contatti-2.jpeg')">
    <div class="mobileScreenFullPageWrapper">
  <?php

  include 'commons/slide-intro.php';
  include 'commons/booking-form.php';
  #include 'commons/icon-scroll.php';
  ?>


  <div class="row m-0">
    <div class="col-md-6">

    </div>
    <div class="col-md-6">
      <div class="d-flex justify-content-center vh100">
        <div class="align-self-center">
          <div class="contactsLocation">
            <?php if($detect->isMobile()){?><div class="cavaliere" style="height:100px;"></div><?php }?>
            <h1>Empoli <img src="<?=$imagesPath?>icon-pin.png" alt=""> </h1>
            <p class="text-white"><img src="<?=$imagesPath?>icon-map-marker.png" alt=""> Via Leonardo da Vinci, 115<br>50059 - Sovigliana-Vinci (FI) </p>
            <p class="text-white"><img src="<?=$imagesPath?>icon-phone.png" alt=""> +39 0571 50199 </p>
            <p class="text-white"><img src="<?=$imagesPath?>icon-mail.png" alt=""> info@scarlettcafe.it<br>direzione@scarlettcafe.it</p>
          </div>
          <div class="contactsLocation">
            <h1>Pontedera <img src="<?=$imagesPath?>icon-pin.png" alt=""> </h1>
            <p class="text-white"><img src="<?=$imagesPath?>icon-map-marker.png" alt=""> Piazza Garibaldi 7,<br>56025 - Pontedera (PI)</p>
            <p class="text-white"><img src="<?=$imagesPath?>icon-phone.png" alt=""> +39 0571 501990</p>
            <p class="text-white"><img src="<?=$imagesPath?>icon-mail.png" alt=""> info@scarlettcafe.it<br>direzione@scarlettcafe.it</p>
          </div>
        </div>
      </div>
    </div>
  </div>
    </div>
</div>
