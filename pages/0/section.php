<?php
$sections = [
  [
    'title' => 'Home',
    'index' => 0,
    'background' => 'imm-homepage.png',
  ],
  [
    'title' => 'Team',
    'index' => 1,
    'background' => 'background-0-1.png'
  ],
  [
    'title' => 'Perché è speciale',
    'index' => 2,
    'background' => 'background-0-1.png'
  ],
  [
    'title' => 'Dicono di noi',
    'index' => 3,

  ]
];
$activSectionIndex = 0;
?>
<script type="text/javascript">
  var sections = <?=json_encode($sections)?>;
</script>
<div id="fullpage">
  <?php foreach($sections as $section){?>
    <div class="section <?=($section['index'] == $activSectionIndex) ? 'active' : ''?>" id="section<?=$section['index']?>" data-anchor="sectionAnchor<?=$section['index']?>">
      <?php
      if(isset($section['slides'])){
        if(count($section['slides'])>0){
          foreach($section['slides'] as $slide){?>
            <div class="fullpageslide">
              <div
              style="<?=isset($slide['background']) ? 'background-size:cover; background-position:center; background-repeat:no-repeat; background-image:url(\''.$imagesPath.$slide['background'].'\')' :  ''?>"
              class=""
              >
              <div class="<?=(isset($slide['extraClasses'])) ? $slide['extraClasses'] : ''?>" data-ancor="slideAnchor<?=$section['index']?>-<?=$slide['index']?>">
                <?php include 'sections/'.$section['index'].'/slides/'.$slide['index'].'/slide.php';?>
              </div>
            </div>
          </div>
          <?php
        }
      }
    }else{?>
      <div
      style="<?=isset($section['background']) ? 'background-size:cover; background-position:center; background-repeat:no-repeat; background-image:url(\''.$siteUrl.$imagesPath.$section['background'].'\')' :  ''?>"
      class="h-100 w-100"
      >

      <?php
      include 'sections/'.$section['index'].'/section.php';
      ?>
    </div>
    <?php
  }
  ?>
</div>
<?php }?>
</div>
