<?php


$slideIntro = [
  'h1' => 'Pizza e Tiramisù',
  'h2' => 'Scarlett',
  'h3' => 'Il nostro team di esperti<br>a vostra disposizione'
];

$pageIntro = [
  'h1' => 'Team',
  'h2' => 'Working Style',
  'p' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.'
];

$icons = [
  [
    'icon'  =>'ws-icon-0.png',
    'title' => 'La nostra storia',
    'content' => 'Grazie alla passione/dedizione di tutto il nostro staff siamo riusciti ad ottenere un prodotto finale di altissimo livello.'
  ],
  [
    'icon' => 'ws-icon-1.png',
    'title' => 'Il nostro stile',
    'content' => 'Grazie alla passione/dedizione di tutto il nostro staff siamo riusciti ad ottenere un prodotto finale di altissimo livello.'
  ]
  ,
  [
    'icon' => 'ws-icon-2.png',
    'title' => 'Il nostro team',
    'content' => 'Grazie alla passione/dedizione di tutto il nostro staff siamo riusciti ad ottenere un prodotto finale di altissimo livello.'
  ]
  ,
  [
    'icon' => 'ws-icon-3.png',
    'title' => 'Lavora con noi',
    'content' => 'Grazie alla passione/dedizione di tutto il nostro staff siamo riusciti ad ottenere un prodotto finale di altissimo livello.<br><br><b><a target="_blank" class="red text-uppercase " href="https://scarlettcafe.guru.jobs/hrJobApplication/select">Candidati subito</a></b>'
  ]
]

?>


<div class="w-100 position-relative bigSlide" style="background-image:url('<?=$siteUrl?>assets/images/imm-team-3<?=($detect->isMobile()) ? '-mobile' : ''?>.jpeg'); <?=($detect->isMobile()) ? 'background-size:contain; background-position: center 15%' : ''?>">




  <div class="mobileScreenFullPageWrapper">
    <?php

    include 'commons/slide-intro.php';

    include 'commons/booking-form.php';
    ?>
  </div>

  <?php include 'commons/icon-scroll.php';?>
</div>

<div class="bg-white">
  <?php   include 'commons/page-intro.php'; ?>
  <?php   include 'commons/icons-group-md3.php'; ?>
  <?php
  $pageIntro = [
    'h1' => 'Team',
    'h2' => 'Il nostro staff',
    'p' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.'
  ];
  ?>
  <?php   #include 'commons/page-intro.php'; ?>
  <!--
  <img class="img-fluid" src="<?=$imagesPath?>staff-img-bw.png" alt="">
-->
<?php
$pageIntro = [
  'h1' => 'Team',
  'h2' => 'Dicono di noi ',
  'p' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.'
];
?>
<?php   include 'commons/page-intro.php'; ?>

<?php

$reviews = [
  [
    'stars' => 4,
    'content' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.',
    'reviewer' => [
      'name' => 'Rubens Parenti',
      'title' => 'Guest'
    ]
  ],
  [
    'stars' => 4,
    'content' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.',
    'reviewer' => [
      'name' => 'Rubens Parenti',
      'title' => 'Guest'
    ]
  ],
  [
    'stars' => 4,
    'content' => 'Nata da un\'idea e un\'intuizione sull\'andamento del settore Food lo Scarlett nel 2016 è stato protagonista di un grosso restyling che è andato a modificare tutto l\'ambiente in uno stile Industry e passando da Locale/Pub a vera e propria Pizzeria.',
    'reviewer' => [
      'name' => 'Rubens Parenti',
      'title' => 'Guest'
    ]
  ]
]
?>
<?php   include 'commons/reviews-boxes.php'; ?>

</div>
